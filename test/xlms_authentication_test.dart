import 'package:flutter_test/flutter_test.dart';

import 'package:xlms_authentication/xlms_authentication.dart';

import 'package:flutter/foundation.dart';
import 'package:http/http.dart' as http;

void main() {
  var xlmsServices = XlmsService(xlmsUrl: 'https://xlms.myworkspace.vn');

  test('Test authentication #1', () async {
    http.Response response = await xlmsServices.authenticate('demo', 'demo');

    if (kDebugMode) {
      print('body=${response.body}');
    }

    expect(response.statusCode == 200 || response.statusCode == 201, true);
    expect(xlmsServices.token!.length, 36);

  });

  test('Test authentication #2', () {
    xlmsServices.authenticate('demo', 'demo')
        .then((response) {
      if (kDebugMode) {
        print('body=${response.body}');
      }
      expect(response.statusCode == 200 || response.statusCode == 201, true);

      var token = xlmsServices.token;
      expect(token!.length, 36);

      print('value=$token');

      String? jsessionId = xlmsServices.parseCookieJSessionID(response);
      print('sessionId=$jsessionId');
      print('Headers:');
      for (var key in response.headers.keys) {
        var value = response.headers[key];
        print('$key=$value');
      }


    });

  });

  // Refer: https://docs.flutter.dev/cookbook/networking/authenticated-requests
  test('Test checkSession', () async {

    var result = await xlmsServices.checkSession();
    if (kDebugMode) {
      print('Session info:=\n${result.body}');
    }

  });

  test('Test getSites', () async {

    var result = await xlmsServices.getSites();
    if (kDebugMode) {
      print('Test getSites; result:=\n${result.body}');
    }

  });
}